package core

import (
	"context"

	"gitlab.com/ruijc/core/internal/parser"
)

type Parser[F any, T any] interface {
	Parse(context.Context, *F, ...string) (*T, error)

	Parses(context.Context, *[]*F, ...string) ([]*T, error)
}

func NewParser[F any, T any]() *parser.Builder[F, T] {
	return parser.NewBuilder[F, T]()
}
