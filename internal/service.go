package internal

import (
	"net/http"
	"reflect"

	"github.com/goexl/exception"
	"github.com/goexl/gox"
	"github.com/goexl/gox/field"
	"github.com/goexl/log"
	"github.com/goexl/mengpo"
	"github.com/goexl/structer"
	"github.com/goexl/xiren"
	"gitlab.com/ruijc/core/internal/hook"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type Service struct {
	logger log.Logger
}

func newService(logger log.Logger) *Service {
	return &Service{
		logger: logger,
	}
}

func (s *Service) Debug(msg string, fields ...gox.Field[any]) {
	s.logger.Debug(msg, fields...)
}

func (s *Service) Info(msg string, fields ...gox.Field[any]) {
	s.logger.Info(msg, fields...)
}

func (s *Service) Warn(msg string, fields ...gox.Field[any]) {
	s.logger.Warn(msg, fields...)
}

func (s *Service) Error(msg string, fields ...gox.Field[any]) {
	s.logger.Error(msg, fields...)
}

func (s *Service) Panic(msg string, fields ...gox.Field[any]) {
	s.logger.Panic(msg, fields...)
}

func (s *Service) Check(req any, name string) (err error) {
	fields := gox.Fields[any]{
		field.New("name", name),
		field.New("req", req),
	}
	s.logger.Debug("收到请求", fields...)

	if me := mengpo.New().Build().Set(req); nil != me {
		err = s.Invalidation(me)
		s.logger.Warn("设置默认值出错", fields.Add(field.Error(me))...)
	} else if xe := xiren.Struct(req); nil != xe {
		err = s.Invalidation(xe)
		s.logger.Warn("数据验证出错", fields.Add(field.Error(xe))...)
	}

	return
}

func (s *Service) Fill(from any, to any, name string, hooks ...hook.Type) (err error) {
	clone := structer.Copy().From(from).To(to).Struct()
	for _, _hook := range hooks {
		clone = clone.Type(s.hookConvert(_hook))
	}
	if ce := s.Check(from, name); nil != ce {
		err = ce
	} else if ae := clone.Build().Apply(); nil != ae {
		err = s.Issue(ae)
		s.logger.Warn("数据转换出错", field.New("to", to), field.Error(ae))
	}

	return
}

func (s *Service) Invalidation(err error) error {
	return s.error(http.StatusBadRequest, err)
}

func (s *Service) Issue(err error) error {
	return s.error(http.StatusInternalServerError, err)
}

func (s *Service) Exception(code exception.Code, fields ...gox.Field[any]) (err error) {
	message := "服务器错误，客户端需要根据返回中的`code`码来确认具体是什么错误"
	exc := exception.New().Code(code).Message(message).Field(fields...).Build()
	err = s.error(http.StatusInternalServerError, exc)

	return
}

func (s *Service) Notfound(fields ...gox.Field[any]) error {
	return s.error(http.StatusNotFound, exception.New().Message("未找到资源").Field(fields...).Build())
}

func (s *Service) Blank(fields ...gox.Field[any]) error {
	return s.error(http.StatusNoContent, exception.New().Message("未找到资源").Field(fields...).Build())
}

func (s *Service) Unauthorized(fields ...gox.Field[any]) error {
	return s.error(http.StatusUnauthorized, exception.New().Message("当前用户未认证，请登录后继续操作").Field(fields...).Build())
}

func (s *Service) Forbidden(fields ...gox.Field[any]) error {
	return s.error(http.StatusForbidden, exception.New().Message("当前用户无权限操作，请联系管理员").Field(fields...).Build())
}

func (s *Service) Conflict(fields ...gox.Field[any]) error {
	return s.error(http.StatusConflict, exception.New().Message("资源已存在").Field(fields...).Build())
}

func (s *Service) Gone(fields ...gox.Field[any]) error {
	return s.error(http.StatusGone, exception.New().Message("资源已不存在").Field(fields...).Build())
}

func (s *Service) Choice(fields ...gox.Field[any]) error {
	return s.error(http.StatusMultipleChoices, exception.New().Message("有多个资源，请选择其中一个继续下一步流程").Field(fields...).Build())
}

func (s *Service) error(code codes.Code, err error) error {
	return status.Error(code, err.Error())
}

func (s *Service) hookConvert(hook hook.Type) func(reflect.Type, reflect.Type, any) (any, error) {
	return hook
}
