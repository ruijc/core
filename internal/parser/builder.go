package parser

import (
	"gitlab.com/ruijc/core/internal/hook"
	"gitlab.com/ruijc/core/internal/parser/internal"
)

type Builder[F any, T any] struct {
	params *internal.Param[F, T]
}

func NewBuilder[F any, T any]() *Builder[F, T] {
	return &Builder[F, T]{
		params: internal.NewParam[F, T](),
	}
}

func (b *Builder[F, T]) Callback(callback internal.Callback[F, T]) (builder *Builder[F, T]) {
	b.params.Callback = callback
	builder = b

	return
}

func (b *Builder[F, T]) Parser(parser internal.Parser[F, T]) (builder *Builder[F, T]) {
	b.params.Parser = parser
	builder = b

	return
}

func (b *Builder[F, T]) Tag(tag string) (builder *Builder[F, T]) {
	b.params.Tag = tag
	builder = b

	return
}

func (b *Builder[F, T]) Type(hook hook.Type) (builder *Builder[F, T]) {
	b.params.Types = append(b.params.Types, hook)
	builder = b

	return
}

func (b *Builder[F, T]) Kind(hook hook.Kind) (builder *Builder[F, T]) {
	b.params.Kinds = append(b.params.Kinds, hook)
	builder = b

	return
}

func (b *Builder[F, T]) Value(hook hook.Value) (builder *Builder[F, T]) {
	b.params.Values = append(b.params.Values, hook)
	builder = b

	return
}

func (b *Builder[F, T]) Build() *internal.Default[F, T] {
	return internal.NewDefault[F, T](b.params)
}
