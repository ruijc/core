package internal

import (
	"context"
)

type Parser[F any, T any] interface {
	Parse(context.Context, *F, *T, ...string) error
}
