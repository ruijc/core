package internal

import (
	"gitlab.com/ruijc/core/internal/hook"
)

type Param[F any, T any] struct {
	Callback Callback[F, T]
	Parser   Parser[F, T]
	Types    []hook.Type
	Kinds    []hook.Kind
	Values   []hook.Value
	Tag      string
}

func NewParam[F any, T any]() *Param[F, T] {
	return &Param[F, T]{
		Types:  make([]hook.Type, 0),
		Kinds:  make([]hook.Kind, 0),
		Values: make([]hook.Value, 0),
		Tag:    "struct",
	}
}
