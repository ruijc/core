package internal

import (
	"context"
	"reflect"

	"github.com/goexl/structer"
	"gitlab.com/ruijc/core/internal/hook"
	"gitlab.com/ruijc/core/internal/parser/internal/internal"
)

type Default[F any, T any] struct {
	params *Param[F, T]
}

func NewDefault[F any, T any](params *Param[F, T]) *Default[F, T] {
	return &Default[F, T]{
		params: params,
	}
}

func (d *Default[F, T]) Parse(ctx context.Context, from *F, fields ...string) (to *T, err error) {
	to = new(T)
	builder := structer.Copy().From(from).To(to).Tag(d.params.Tag)
	for _, typ := range d.params.Types {
		builder = builder.Type(d.convertType(typ))
	}
	for _, kind := range d.params.Kinds {
		builder = builder.Kind(d.convertKind(kind))
	}
	for _, value := range d.params.Values {
		builder = builder.Value(d.convertValue(value))
	}
	if nil != d.params.Parser {
		switch parse := d.params.Parser.(type) {
		case internal.Type:
			builder = builder.Type(parse.Type)
		case internal.Kind:
			builder = builder.Kind(parse.Kind)
		case internal.Value:
			builder = builder.Value(parse.Value)
		}
	}

	if ce := builder.Build().Apply(); nil != ce {
		err = ce
	} else if nil != d.params.Callback {
		err = d.params.Callback(ctx, from, to, fields...)
	} else if nil != d.params.Parser {
		err = d.params.Parser.Parse(ctx, from, to, fields...)
	}

	return
}

func (d *Default[F, T]) Parses(ctx context.Context, from *[]*F, fields ...string) (to []*T, err error) {
	to = make([]*T, 0, len(*from))
	for _, user := range *from {
		cloned := user
		if parsed, pe := d.Parse(ctx, cloned, fields...); nil != pe {
			err = pe
		} else {
			to = append(to, parsed)
		}

		if nil != err {
			break
		}
	}

	return
}

func (d *Default[F, T]) convertType(hook hook.Type) func(reflect.Type, reflect.Type, any) (any, error) {
	return func(ft reflect.Type, tt reflect.Type, from any) (any, error) {
		return hook(ft, tt, from)
	}
}

func (d *Default[F, T]) convertKind(hook hook.Kind) func(reflect.Kind, reflect.Kind, any) (any, error) {
	return func(fk reflect.Kind, tk reflect.Kind, from any) (any, error) {
		return hook(fk, tk, from)
	}
}

func (d *Default[F, T]) convertValue(hook hook.Value) func(reflect.Value, reflect.Value) (any, error) {
	return func(from reflect.Value, to reflect.Value) (any, error) {
		return hook(from, to)
	}
}
