package internal

import (
	"reflect"
)

type Value interface {
	Value(from reflect.Value, to reflect.Value) (any, error)
}
