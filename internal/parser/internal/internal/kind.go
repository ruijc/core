package internal

import (
	"reflect"
)

type Kind interface {
	Kind(reflect.Kind, reflect.Kind, any) (any, error)
}
