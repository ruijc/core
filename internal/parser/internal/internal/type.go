package internal

import (
	"reflect"
)

type Type interface {
	Type(reflect.Type, reflect.Type, any) (any, error)
}
