package internal

import (
	"context"
)

type Callback[F any, T any] func(context.Context, *F, *T, ...string) error
